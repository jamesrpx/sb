Developed on Windows with IntelliJ, requires the IntelliJ lombok plugin.<br>
Build in IntelliJ or using mvn.<br>
A "fat" executable jar containing all dependencies will be created in the target directory<br>
To run from the command line
<code>
java -jar target/sb-1.0-SNAPSHOT-jar-with-dependencies.jar
</code>
The URL for the source page can be provided as a command line argument, this will default to
<b>http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html</b>
if omitted.<br>
Debug logging is written to logs/sb.log
