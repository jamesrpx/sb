package sbtechtest.util;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class ProductDescriptionElementBuilder {
    private Document doc;
    private Element productDescriptionElement;

    public ProductDescriptionElementBuilder(Document doc) {
        this.doc = doc;
        productDescriptionElement = doc.createElement("div");
    }

    public Element build() {
        return productDescriptionElement;
    }

    public ProductDescriptionElementBuilder withDescription(String description) {
        Element header = doc.createElement("h3").addClass("productDataItemHeader").appendText("Description");
        productDescriptionElement.appendChild(header);
        Element productText = doc.createElement("div").addClass("productText").appendText(description);
        productDescriptionElement.appendChild(productText);

        return this;
    }
}
