package sbtechtest.util;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class ProductElementBuilder {
    private Document doc;
    private Element productElement;

    public ProductElementBuilder(Document doc) {
        this.doc = doc;
        productElement = doc.createElement("div").addClass("product");
    }

    public Element build() {
        return productElement;
    }

    public ProductElementBuilder withUnitPrice(String unitPrice) {
        Element pricing = doc.createElement("div");
        pricing.addClass("pricing");
        Element p = doc.createElement("p");
        p.addClass("pricePerUnit");
        p.appendText(unitPrice);
        pricing.appendChild(p);
        productElement.appendChild(pricing);

        return this;
    }

    public ProductElementBuilder withTitle(String title) {
        Element productInfo = doc.createElement("div");
        productInfo.addClass("productInfo");
        Element h3 = doc.createElement("h3");
        Element a = doc.createElement("a");
        a.appendText(title);
        productInfo.appendChild(h3);
        h3.appendChild(a);
        productElement.appendChild(productInfo);

        return this;
    }

}
