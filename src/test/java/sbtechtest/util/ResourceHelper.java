package sbtechtest.util;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class ResourceHelper {
    public static String loadResource(String name) throws Exception {
        return Files.lines(Paths.get(ResourceHelper.class.getClassLoader().getResource(name).toURI())).collect(Collectors.joining("\n"));
    }
}
