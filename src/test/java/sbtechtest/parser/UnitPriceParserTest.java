package sbtechtest.parser;

import org.junit.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class UnitPriceParserTest {
    private UnitPriceParser unitPriceParser = new UnitPriceParser();

    @Test
    public void testParseValidTextSuccess() {
        BigDecimal actual = unitPriceParser.parseUnitPrice("&pound1.80/unit");
        assertThat(actual.toString()).isEqualTo("1.80");
    }

    @Test(expected = ProductParserException.class)
    public void testParseInvalidTextFailure() {
        unitPriceParser.parseUnitPrice("£3.50");
    }
}
