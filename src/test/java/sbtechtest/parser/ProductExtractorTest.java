package sbtechtest.parser;

import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import sbtechtest.comms.UrlReader;
import sbtechtest.domain.Product;
import sbtechtest.util.ProductElementBuilder;
import sbtechtest.util.ResourceHelper;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class ProductExtractorTest {
    private static final String TEST_URL = "http://localhost/test";
    private static final String TEST_TITLE = "test product";
    private static final String TEST_UNIT_PRICE_TEXT = "&pound3.50/unit";
    private static final String TEST_DESCRIPTION = "Test description";
    private static final int TEST_CONTENT_LENGTH = 12345;
    private static final BigDecimal TEST_UNIT_PRICE = BigDecimal.valueOf(3.5).setScale(2, BigDecimal.ROUND_HALF_EVEN);
    private static final String DESCRIPTION_HTML_SOURCE = "doc_product_description.html";

    @Mock
    private UrlReader urlReader;

    private ProductExtractor productExtractor;
    private Document doc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        productExtractor = ProductExtractor.builder()
                .descriptionParser(new HtmlParser<>(new ProductDescriptionDocumentProcessor()))
                .unitPriceParser(new UnitPriceParser())
                .urlReaderSupplier(url -> urlReader)
                .build();

        doc = new Document(TEST_URL);
    }

    @Test
    public void extractProductSuccess() throws Exception {
        when(urlReader.getContent()).thenReturn(ResourceHelper.loadResource(DESCRIPTION_HTML_SOURCE));
        when(urlReader.getContentLength()).thenReturn(TEST_CONTENT_LENGTH);

        doc = new Document(TEST_URL);
        Product product = productExtractor.extractProduct(
                new ProductElementBuilder(doc)
                    .withTitle(TEST_TITLE)
                    .withUnitPrice(TEST_UNIT_PRICE_TEXT)
                    .build());

        assertThat(product.getTitle()).isEqualTo(TEST_TITLE);
        assertThat(product.getProductDetails().getDescription()).isEqualTo(TEST_DESCRIPTION);
        assertThat(product.getProductDetails().getSize()).isEqualTo(TEST_CONTENT_LENGTH);
        assertThat(product.getUnitPrice()).isEqualTo(TEST_UNIT_PRICE);
    }

    @Test(expected = ProductParserException.class)
    public void extractProductMissingTitleFailure() {
        productExtractor.extractProduct(
                new ProductElementBuilder(doc)
                        .withUnitPrice(TEST_UNIT_PRICE_TEXT)
                        .build());
    }

    @Test(expected = ProductParserException.class)
    public void extractProductMissingUnitPriceFailure() throws Exception {
        when(urlReader.getContent()).thenReturn(ResourceHelper.loadResource(DESCRIPTION_HTML_SOURCE));

        productExtractor.extractProduct(
                new ProductElementBuilder(doc)
                        .withTitle(TEST_TITLE)
                        .build());
    }

    @Test(expected = ProductParserException.class)
    public void extractProductMissingDescriptionFailure() {
        productExtractor.extractProduct(
                new ProductElementBuilder(doc)
                        .withTitle(TEST_TITLE)
                        .withUnitPrice(TEST_UNIT_PRICE_TEXT)
                        .build());
    }
}
