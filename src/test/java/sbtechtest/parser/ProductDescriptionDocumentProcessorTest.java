package sbtechtest.parser;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Test;
import sbtechtest.util.ProductDescriptionElementBuilder;

import static org.assertj.core.api.Assertions.assertThat;

public class ProductDescriptionDocumentProcessorTest {
    private static final String TEST_URL = "http://localhost:80/test.html";
    private static String TEST_DESCRIPTION = "Test description";

    @Test(expected = ProductParserException.class)
    public void extractDescriptionNotFoundThrowsException() {
        ProductDescriptionDocumentProcessor processor = new ProductDescriptionDocumentProcessor();
        processor.processDocument(new Document(TEST_URL));
    }

    @Test
    public void parseProductDescriptionSuccess() throws Exception {
        Document doc = new Document(TEST_URL);
        Element productDescriptionElement =
            new ProductDescriptionElementBuilder(doc)
                .withDescription(TEST_DESCRIPTION)
                .build();
        doc.appendChild(productDescriptionElement);

        ProductDescriptionDocumentProcessor processor = new ProductDescriptionDocumentProcessor();
        String description = processor.processDocument(doc);
        assertThat(description).isEqualTo(TEST_DESCRIPTION);
    }
}
