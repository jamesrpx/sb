package sbtechtest.parser;

import lombok.extern.java.Log;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import sbtechtest.domain.Product;
import sbtechtest.domain.ProductDocument;
import sbtechtest.util.ProductElementBuilder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ProductDocumentProcessorTest {
    private static final String TEST_URL = "http://localhost:80/test.html";
    private static final String TEST_TITLE = "Test product";

    @Mock
    private ProductExtractor productExtractor;

    private ProductDocumentProcessor documentParser;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        documentParser = ProductDocumentProcessor.builder()
                .productExtractor(productExtractor)
                .build();
    }

    @Test
    public void parseProductDocumentSuccess() {
        Document doc = new Document(TEST_URL);
        doc.appendChild(new ProductElementBuilder(doc).withTitle(TEST_TITLE).build());
        doc.appendChild(new ProductElementBuilder(doc).withTitle(TEST_TITLE).build());

        when(productExtractor.extractProduct(any(Element.class))).thenReturn(Product.builder().build());

        ProductDocument productDocument = documentParser.processDocument(doc);
        assertThat(productDocument.getProducts()).hasSize(2);
    }

    @Test
    public void parseProductDocumentNoProductsFoundSuccess() throws Exception {
        Document doc = new Document(TEST_URL);

        ProductDocument productDocument = documentParser.processDocument(doc);
        assertThat(productDocument.getProducts()).hasSize(0);
    }
}
