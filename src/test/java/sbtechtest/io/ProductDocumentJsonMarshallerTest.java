package sbtechtest.io;

import org.junit.Test;
import sbtechtest.domain.Product;
import sbtechtest.domain.ProductDetails;
import sbtechtest.domain.ProductDocument;
import sbtechtest.util.ResourceHelper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;

import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson;

public class ProductDocumentJsonMarshallerTest {
    private static final String EXPECTED_JSON_SOURCE = "doc_2_products.json";

    @Test
    public void testToJsonSuccess() throws Exception {
        List<Product> products = new LinkedList<>();
        products.add(getProduct("Product 1", 1500, "1.5", "Description 1"));
        products.add(getProduct("Product 2", 2500, "2.5", "Description 2"));
        String output = ProductDocumentJsonMarshaller.toJson(new ProductDocument(products));
        String expected = ResourceHelper.loadResource(EXPECTED_JSON_SOURCE);

        assertThatJson(output).isEqualTo(expected);
    }

    private Product getProduct(String title, int size, String unitPrice, String description) {
        return Product.builder()
                .title(title)
                .unitPrice(new BigDecimal(unitPrice).setScale(2, RoundingMode.HALF_EVEN))
                .productDetails(ProductDetails.builder()
                                    .description(description)
                                    .size(size)
                                    .build())
                .build();
    }
}