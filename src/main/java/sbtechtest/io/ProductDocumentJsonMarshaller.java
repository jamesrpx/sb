package sbtechtest.io;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import sbtechtest.domain.Product;
import sbtechtest.domain.ProductDetails;
import sbtechtest.domain.ProductDocument;
import sbtechtest.parser.ProductParserException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProductDocumentJsonMarshaller {
    private static final String RESULTS_PROPERTY = "results";
    private static final String TITLE_PROPERTY = "title";
    private static final String SIZE_PROPERTY = "size";
    private static final String SIZE_FORMAT = "%skb";
    private static final String UNIT_PRICE_PROPERTY = "unit_price";
    private static final String DESCRIPTION_PROPERTY = "description";
    private static final String TOTAL_PROPERTY = "total";

    /**
     * Marshall document into JSON representation.
     * A Map is created to mirror the required properties of the JSON schema, using
     * a LinkedHashMap is used to preserve ordering of properties.
     *
     * @param doc the ProductDocument to marshall
     * @return String, the JSON string representing the object
     */
    public static String toJson(ProductDocument doc) {
        Map<String, Object> objectMap = new LinkedHashMap<>();
        objectMap.put(RESULTS_PROPERTY, getResults(doc));
        objectMap.put(TOTAL_PROPERTY, getTotal(doc));

        return getOutput(objectMap);
    }

    private static List<Map<String, Object>> getResults(ProductDocument productDocument) {
        return productDocument.getProducts().stream()
                .map(ProductDocumentJsonMarshaller::getProductPropertiesMap)
                .collect(Collectors.toList());
    }

    private static Map<String, Object> getProductPropertiesMap(Product product) {
        Map<String, Object> properties = new LinkedHashMap<>();
        ProductDetails productDetails = product.getProductDetails();

        properties.put(TITLE_PROPERTY, product.getTitle());
        properties.put(SIZE_PROPERTY, formatSize(productDetails.getSize()));
        properties.put(UNIT_PRICE_PROPERTY, product.getUnitPrice());
        properties.put(DESCRIPTION_PROPERTY, productDetails.getDescription());

        return properties;
    }

    private static String formatSize(double size) {
        BigDecimal sizeKb = BigDecimal.valueOf(size / 1000).setScale(1, RoundingMode.HALF_EVEN).stripTrailingZeros();
        return String.format(SIZE_FORMAT, sizeKb.toPlainString());
    }


    private static BigDecimal getTotal(ProductDocument doc) {
        return doc.getTotal();
    }

    private static String getOutput(Map<String, Object> objectMap) {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter()
                    .writeValueAsString(objectMap);
        } catch (JsonProcessingException x) {
            throw new ProductParserException("marshalling to JSON failed", x);
        }
    }
}
