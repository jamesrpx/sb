package sbtechtest.comms;

import sbtechtest.parser.ProductParserException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.stream.Collectors;

public class UrlReader {
    private String url;
    private URLConnection connection;

    public UrlReader(String url) throws Exception {
        this.url = url;
        connection = new URL(url).openConnection();
        connection.connect();
    }

    public int getContentLength() {
        return connection.getContentLength();
    }

    public String getContent() {
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String content = in.lines().collect(Collectors.joining("\n"));
            in.close();

            return content;
        } catch (Exception x) {
            throw new ProductParserException(String.format("error reading url '%s'", url), x);
        }
    }

    @FunctionalInterface
    public interface UrlReaderSupplier {
        UrlReader get(String url);
    }
}
