package sbtechtest.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@Builder
@Getter
@ToString
public class Product {
    private String title;
    private ProductDetails productDetails;
    private BigDecimal unitPrice;
}
