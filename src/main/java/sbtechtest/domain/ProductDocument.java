package sbtechtest.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@Getter
public class ProductDocument {
    private List<Product> products;

    public BigDecimal getTotal() {
        return products.stream().map(Product::getUnitPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
