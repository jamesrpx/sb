package sbtechtest.domain;

import lombok.Builder;
import lombok.Getter;

/**
 * Class holding details of a product
 */
@Builder
@Getter
public class ProductDetails {
    private String description;
    private int size;
}
