package sbtechtest;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sbtechtest.comms.UrlReader;
import sbtechtest.domain.ProductDocument;
import sbtechtest.io.ProductDocumentJsonMarshaller;
import sbtechtest.parser.HtmlParser;
import sbtechtest.parser.ProductDescriptionDocumentProcessor;
import sbtechtest.parser.ProductDocumentProcessor;
import sbtechtest.parser.ProductExtractor;
import sbtechtest.parser.ProductParserException;
import sbtechtest.parser.UnitPriceParser;

@Slf4j
@AllArgsConstructor
public class RipeAndReadyApp {
    private static final String DEFAULT_URL = "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html";

    private String url;

    private void run() {
        HtmlParser<ProductDocument> productDocumentParser =
            new HtmlParser<>(
                ProductDocumentProcessor.builder()
                    .productExtractor(ProductExtractor.builder()
                        .descriptionParser(new HtmlParser<>(new ProductDescriptionDocumentProcessor()))
                        .unitPriceParser(new UnitPriceParser())
                        .urlReaderSupplier(this::getUrlReader)
                        .build())
                    .build());

        String source = getUrlReader(url).getContent();
        ProductDocument productDocument = productDocumentParser.parse(source);
        String output = getOutput(productDocument);
        log.debug(output);
        System.out.println(output);
    }

    private String getOutput(ProductDocument productDocument) {
        return ProductDocumentJsonMarshaller.toJson(productDocument);
    }

    public static void main(String... args) {
        String url = args.length < 1 ? DEFAULT_URL : args[0];

        try {
            new RipeAndReadyApp(url).run();
        } catch (ProductParserException x) {
            String error = String.format("Error processing '%s'", url);
            log.error(error, x);
            System.err.println(String.format("%s: %s", error, x.getMessage()));
        }
    }

    private UrlReader getUrlReader(String url) {
        try {
            return new UrlReader(url);
        } catch (Exception x) {
            throw new ProductParserException(String.format("error reading url %s", url), x);
        }
    }
}
