package sbtechtest.parser;

/**
 * Exception thrown if an error occurs during parsing of the products, contains a message describing the error
 */
public class ProductParserException extends RuntimeException {
    public ProductParserException(String reason) {
        super(reason);
    }

    public ProductParserException(String reason, Exception cause) {
        super(reason, cause);
    }
}
