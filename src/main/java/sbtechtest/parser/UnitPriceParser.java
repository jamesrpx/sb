package sbtechtest.parser;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parser for unit prices in the expected format of "&pound{value}/unit" where {value} is the price in GBP with two decimal places.
 */
public class UnitPriceParser {
    private static final Pattern UNIT_PRICE_PATTERN = Pattern.compile("&pound(\\d+\\.\\d\\d)/unit");

    BigDecimal parseUnitPrice(String unitPriceText) {
        Matcher matcher = UNIT_PRICE_PATTERN.matcher(unitPriceText);

        if (!matcher.matches()) {
            throw new ProductParserException(String.format("cannot extract unit price from '%s'", unitPriceText));
        }

        return new BigDecimal(matcher.group(1)).setScale(2, RoundingMode.HALF_EVEN);
    }
}
