package sbtechtest.parser;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Abstract class defining strategy HTML document parsing.
 * Converts the input HTML into a Jsoup Document and calls the abstract processDocument method on the Document
 */
@Slf4j
@AllArgsConstructor
public class HtmlParser<T> {
    private static final int MAX_DEBUG_LEN = 80;

    private DocumentProcessor<T> documentProcessor;

    /**
     * Parses a string containing HTML into a T instance
     *
     * @param html the input HTML
     * @return T
     * @throws ProductParserException if an error occurs
     */
    public final T parse(String html) {
        log.debug("parsing html '{}'", StringUtils.abbreviate(html, MAX_DEBUG_LEN));

        try {
            Document doc = Jsoup.parse(html);

            return documentProcessor.processDocument(doc);
        } catch (Exception x) {
            throw new ProductParserException("failed to parse html", x);
        }
    }
}
