package sbtechtest.parser;


import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import sbtechtest.domain.Product;
import sbtechtest.domain.ProductDocument;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Extracts all elements with CSS class "product" from a Jsoup Document and builds a ProductDocument instance
 * containing a list of Products
 */
@Slf4j
@Builder
public class ProductDocumentProcessor implements DocumentProcessor<ProductDocument> {
    private ProductExtractor productExtractor;

    @Override
    public ProductDocument processDocument(Document doc)  {
        Elements productElements = doc.getElementsByClass("product");
        log.debug("found {} product elements", productElements.size());

        List<Product> productList = extractProducts(productElements);

        return new ProductDocument(productList);
    }

    private List<Product> extractProducts(Elements productElements) {
        return productElements.stream().map(productExtractor::extractProduct).collect(Collectors.toList());
    }
}
