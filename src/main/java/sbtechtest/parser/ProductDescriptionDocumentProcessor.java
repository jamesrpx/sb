package sbtechtest.parser;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * Parser for product details document
 */
public class ProductDescriptionDocumentProcessor implements DocumentProcessor<String> {
    private static final String DESCRIPTION_SELECTOR =
            "h3.productDataItemHeader:matches(Description) + div.productText";

    @Override
    public String processDocument(Document doc) {
        Elements selectedElements = doc.select(DESCRIPTION_SELECTOR);

        if (selectedElements == null || selectedElements.size() == 0) {
            throw new ProductParserException("Description element not found");
        }

        return selectedElements.first().text();
    }
}
