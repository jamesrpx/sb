package sbtechtest.parser;

import org.jsoup.nodes.Document;

public interface DocumentProcessor<T> {
    T processDocument(Document doc);
}
