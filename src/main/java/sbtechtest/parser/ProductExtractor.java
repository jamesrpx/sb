package sbtechtest.parser;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import sbtechtest.comms.UrlReader;
import sbtechtest.domain.Product;
import sbtechtest.domain.ProductDetails;

import java.math.BigDecimal;

/**
 * Extracts a product instance from a product HTML element.
 */
@Slf4j
@Builder
public class ProductExtractor {
    private static final String PRODUCT_LINK_SELECTOR = "div.productInfo > h3 > a";
    private static final String UNIT_PRICE_SELECTOR = "div.pricing > p.pricePerUnit";

    private HtmlParser<String> descriptionParser;
    private UnitPriceParser unitPriceParser;
    private UrlReader.UrlReaderSupplier urlReaderSupplier;

    Product extractProduct(Element productElement) {
        Element productLink = getProductLink(productElement);

        log.debug("found product '{}'", productLink.text());

        return Product.builder()
                .title(productLink.text())
                .productDetails(getProductDetails(productLink))
                .unitPrice(getUnitPrice(productElement))
                .build();
    }

    private Element getProductLink(Element productElement) {
        return selectFirstElement(productElement, PRODUCT_LINK_SELECTOR, "product link element not found");
    }

    private Element selectFirstElement(Element productElement, String title_selector, String reason) {
        Elements selectedElements = productElement.select(title_selector);

        if (selectedElements == null || selectedElements.size() == 0) {
            throw new ProductParserException(reason);
        }

        return selectedElements.first();
    }

    private ProductDetails getProductDetails(Element productLink) {
        String url = productLink.attr("href");

        log.debug("getting product details from '{}'", url);
        UrlReader urlReader = urlReaderSupplier.get(url);
        String content = urlReader.getContent();

        return ProductDetails.builder()
                .size(urlReader.getContentLength())
                .description(descriptionParser.parse(content))
                .build();
    }

    private BigDecimal getUnitPrice(Element productElement) {
        Element selectedElement = selectFirstElement(productElement, UNIT_PRICE_SELECTOR, "unit price element not found");
        String unitPriceText = selectedElement.text();

        return unitPriceParser.parseUnitPrice(unitPriceText);
    }
}
